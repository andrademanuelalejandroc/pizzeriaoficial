var url =
  "https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json";

async function leerJSON(url) {
  try {
    let response = await fetch(url);
    let user = await response.json();
    return user;
  } catch (err) {
    alert(err);
  }
}

var json = leerJSON(url);

function cantidadBox() {
  let cantidad = document.getElementById("cantidadPizzas").value;
  let opcion = "";
  let reset = "";
  for (var i = 1; i <= cantidad; i++) {
    let box = `
    <div class="row border rounded p-3" style="margin-top:20px;">
      <div class="col-3 offset-1 align-self-center">
        <label>Tamaño de pizza ${i}</label>
      </div>
      <div class="col-3 align-self-center">
        <select name="tama" class="form-control" id="pizza${i}" >
          <option value="1">Grande</option>
          <option value="2">Mediano</option>
          <option value="3">Pequeño</option>
        </select>
      </div>
    </div>`;
    opcion += box;
  }
  opcion += `
  <div class="container row mt-5">
    <div class="col-0 offset-10 mt-2">
      <input type="submit" value="Cargar opciones"  class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off"></input>
    </div>
  </div>`;
  reset = `
  <a href="index.html">
    <button type="button" onclick="cantidadBox()" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">
      reset
    </button>
  </a>
  `;
  document.getElementById("opcionCantidad").innerHTML = opcion;
  document.getElementById("reset").innerHTML = reset;
}

function selectPizza(cantidad, tama) {
  let msj = "";
  for (let i = 0; i < cantidad; i++) {
    msj += `
    <div class="container row border rounded mt-5 pt-4 pb-4 pr-1 pl-1">
    <div class="col-6">
    <div  class="row">
    <div class="col-12">
    <p class="mb-5">Escoja sabora para pizza ${
      i + 1
    } (puede escoger uno o dos):</p>
    </div>
    </div>
    <div class="row">
    <div class="col-12">
    <label id="nombrePizza1${i}" >Ingrediente adicionales (Escogio ninguno)</label><br>
    <div id="box1${i}">
    </div>
    </div>
    </div>
    <div class="row" >
    <div class="col-12">
    <label id="nombrePizza2${i}"  for="">Ingrediente adicionales (Escogio ninguno)</label> <br>
    <div id="box2${i}">
    </div>
    </div>
    </div>
    </div>
    <div class="col-6">
    <div class="row">
    <div class="col">
    <select class="form-control" name="select1" id="nombresPizzas1${i}" onchange="getImagenPizzaSelect(1,${i})" >
    </select>
    </div>
    <div class="col">
    <select class="form-control" name="select2"  id="nombresPizzas2${i}"   onchange="getImagenPizzaSelect(2,${i})">
    </select>
    </div>
    </div>
    <div class="row justify-content-center mt-5">
    <div  id="pizzaImagen1${i}"class="col-6">
    <img class="img-fluid" src="../images/img.png" alt="">
    </div>
    <div class="col-6" id="pizzaImagen2${i}">
    </div>
    </div>
    </div>
    </div>
    <input style="display:none;" type="text" name="tama" value="${tama[i]}">
    `;
    getPizzas(i);
    getImagenPizza(i);
    generarCheckBox(1, i);
    generarCheckBox(2, i);
  }
  msj += `<div class="container row mt-5">
  <div class="col-0 offset-4 mt-2 mb-5">
  <input type="submit" value="Calcular Factura" style="Width:280px; height:50px;" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">
  </input>
  </div>
  </div>`;
  document.getElementById("selectPizza").innerHTML = msj;
}

function generarCheckBox(idBox, idCiclo) {
  var nombreURL = "";
  json.then((pizzeria) => {
    for (let i = 0; i < 4; i++) {
      nombreURL += `
      <div class="form-check form-check-inline">
      <input class="form-check-input" type="checkbox" name="box${
        idBox + "" + idCiclo
      }" id="box${idBox + "" + idCiclo + "" + i}" value="${i}">
      <label style="margin:10px;" class="form-check-label" for="box${
        idBox + "" + idCiclo
      }">${pizzeria.adicional[i].nombre_ingrediente}</label>
      </div>
      `;
    }
    document.getElementById("box" + idBox + idCiclo).innerHTML = nombreURL;
  });
}

function deshabilitarCheckBox(idSelect, idCiclo) {
  var elementos = document.getElementsByName(`box${idSelect + "" + idCiclo}`);
  for (var u = 0; u < elementos.length; u++) {
    console.log(elementos[u]);
    elementos[u].disabled = true;
  }
}

function habilitarCheckBox(idSelect, idCiclo) {
  var elementos = document.getElementsByName(`box${idSelect + "" + idCiclo}`);
  for (var i = 0; i < elementos.length; i++) {
    elementos[i].disabled = false;
  }
}

function getImagenPizzaSelect(idSelect, idCiclo) {
  var nombreURL = "";
  var nombrePizza = "";

  json.then((pizzeria) => {
    if (
      document.getElementById("nombresPizzas" + idSelect + idCiclo).value >= 0
    ) {
      nombreURL += `
      <img class="img-fluid" style="width:400px; height:150px;" src="${
      pizzeria.pizzas[
      document.getElementById("nombresPizzas" + idSelect + idCiclo).value
      ].url_Imagen
    }">
    `;
      nombrePizza = `Ingredientes adicionales (${
      pizzeria.pizzas[
      document.getElementById("nombresPizzas" + idSelect + idCiclo).value
      ].sabor
    })`;
      habilitarCheckBox(idSelect, idCiclo);
    } else {
      deshabilitarCheckBox(idSelect, idCiclo);
      nombrePizza = `Ingrediente adicionales (Escogio ninguno) `;
      nombreURL = `
    <img class="img-fluid" src="">
    `;
    }

    document.getElementById(
      "pizzaImagen" + idSelect + idCiclo
    ).innerHTML = nombreURL;
    document.getElementById(
      `nombrePizza${idSelect}` + idCiclo
    ).innerHTML = nombrePizza;
    for (var i = 0; i < document.getElementById("nombresPizzas2" + idCiclo).length; i++) {
      document.getElementById("nombresPizzas2" + idCiclo)[i].disabled = false;
    }
    let p = parseInt(document.getElementById("nombresPizzas1" + idCiclo).value) + 1;
      document.getElementById("nombresPizzas2" + idCiclo)[p].disabled = true;
  });
}

function getImagenPizza(idImagen) {
  var nombreURL = "";

  json.then((pizzeria) => {
    nombreURL += `
    <img class="img-fluid" style="width:400px; height:150px;"  src="${
      pizzeria.pizzas[
      document.getElementById("nombresPizzas1" + idImagen).value
      ].url_Imagen
    }" alt="">
    `;
    document.getElementById("pizzaImagen1" + idImagen).innerHTML = nombreURL;
  });
}

function getPizzas(idPizza) {
  var nombres = `<option value="-1" selected>Ninguna</option>`;
  var nombre2 = ``;
  json.then((pizzeria) => {
    for (var i = 0; i < pizzeria.pizzas.length; i++) {
      nombres += `
      <option value="${i}">${pizzeria.pizzas[i].sabor}</option>
      `;
    }
    nombre2 += nombres;
    document.getElementById("nombresPizzas1" + idPizza).innerHTML = nombres;
    document.getElementById("nombresPizzas2" + idPizza).innerHTML = nombre2;
  });
}

function MainOpciones() {
  let parametros = new URLSearchParams(location.search);
  selectPizza(parametros.getAll("tama").length, parametros.getAll("tama"));
}

function MainFactura() {
  console.log("main factura");
  let parametros = new URLSearchParams(location.search);
  json.then((pizzeria) => {

    var select1 = parametros.getAll("select1");
    var select2 = parametros.getAll("select2");
    var tamanoPizza = parametros.getAll("tama");
    var factura = "";
    let total = 0;
    for (var i = 0; i < tamanoPizza.length; i++) {

      let tamano = getTamanoPizza(tamanoPizza[i]);
      let sabor = leerSabor(select1[i], select2[i], tamanoPizza[i], pizzeria.pizzas);
      saborPizza = sabor[0];
      sabor[0] = "Pizza " + tamano + " " + sabor[0];
      factura += cadenaSabor(sabor);

      let checkbox1 = parametros.getAll("box" + 1 + "" + i);
      let checkbox2 = parametros.getAll("box" + 2 + "" + i);
      console.log("TAMANO DE CHECKBOXX" + checkbox2.length)
      let checkbox1RTA = leerCheckbox(checkbox1, pizzeria);

      factura += cadenaCheckbox(checkbox1RTA, pizzeria.pizzas[select1[i]].sabor, checkbox1.length);
      var checkbox2RTA;
      if (select2[i] != -1) {
        console.log("OJAAA");
        checkbox2RTA = leerCheckbox(checkbox2, pizzeria);
        console.log("aqui" + checkbox2RTA);
        factura += cadenaCheckbox(checkbox2RTA, pizzeria.pizzas[select2[i]].sabor, checkbox2.length);
        total += calcularTotalPizza(checkbox2RTA, 0);

      }



      console.log("Antes de total() 2rta" + checkbox2RTA);
      total += calcularTotalPizza(checkbox1RTA, sabor[1]);
    }


    factura += cadenaTotal(total);
    document.getElementById("tablaFactura").innerHTML = factura;
  });
}

function cadenaTotal(total) {

  let cadena = `<tr>
    <td>Total :</td>
    <td> $ ${total}</td>
    </tr>`;

  return cadena;
}


function calcularTotalPizza(checkboxRTA, sabor) {
  let total = checkboxRTA[2];
  if (checkbox2RTA = !"undefined") {
    total += checkbox2RTA[1];
  }
  total += sabor;
  console.log("Costo checkBox  :" + checkboxRTA[2]);
  return total;
}


function cadenaCheckbox(checkbox, saborPizza, tamano) {
  let cadena = "";
  console.log("cantidad de box" + checkbox.length);
  for (let i = 0; i < tamano; i++) {
    cadena += `<tr>
    <td>Adicional-${saborPizza}-${checkbox[0][i]}</td>
    <td> $ ${checkbox[1][i]}</td>
    </tr>`;
  }

  return cadena;

}

function leerCheckbox(box, pizzas) {
  let nombresIngredientes = new Array(box.length);
  let precioIngrediente = new Array(box.length);
  let costoTotal = 0;
  let rta = new Array(3);
  console.log(box);
  for (var i = 0; i < box.length; i++) {
    console.log(pizzas.adicional[box[i]].nombre_ingrediente);
    nombresIngredientes[i] = pizzas.adicional[box[i]].nombre_ingrediente;
    precioIngrediente[i] = pizzas.adicional[box[i]].valor;
    costoTotal += pizzas.adicional[box[i]].valor;
  }
  rta[0] = nombresIngredientes;
  rta[1] = precioIngrediente;
  rta[2] = costoTotal;
  return rta;
}



function getTamanoPizza(tamanoPizza) {
  let tamano = "";

  if (tamanoPizza == '1')
    tamano = "Grande";
  if (tamanoPizza == '2')
    tamano = "Mediana"
  if (tamanoPizza == '3')
    tamano = "Pequeña"
  return tamano
}

function cadenaSabor(sabor) {
  console.log("sabor" + sabor[0]);
  let cadena = `<tr>
 <td>${sabor[0]}</td>
 <td> $ ${sabor[1]}</td>
 </tr>`;

  return cadena;
}


function leerSabor(select1, select2, tama, pizzas) {
  let sabores = "";
  let mayor = 0;
  let precio = 0;
  let rta = new Array(2);
  let nombre = "";
  var precioSelect1 = getPrecioPizza(select1, tama, pizzas);
  var precioSelect2 = getPrecioPizza(select2, tama, pizzas);
  precio = Math.max(precioSelect1, precioSelect2);
  if (select2 == -1) {
    nombre = pizzas[select1].sabor;
    console.log(nombre);
  } else {
    nombre = "Mitad " + pizzas[select1].sabor + " y  Mitad " + pizzas[select2].sabor;
    console.log(nombre);
  }
  rta[0] = nombre;
  rta[1] = precio;
  return rta;
}

function getPrecioPizza(select, tama, pizzas) {
  if (select == -1) return -1;
  let precio = pizzas[select].precio[tama - 1].precio;
  return precio;
}

function deshabilitarTodosCheckBox(idCiclo) {
  for (var i = 0; i < idCiclo; i++) {
    console.log("s");
    deshabilitarCheckBox(2, i);
  }
}
