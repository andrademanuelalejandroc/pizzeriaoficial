![Pizzería](http://www.madarme.co/portada-web.png)
# Mi pizzeria
Este proyecto es una aplicación web para hacer un pedido a una pizzería en la cual el usuario puede seleccionar la cantidad de pizzas que desee ordenar, asimismo de cada una de las pizzas puede combinar por mitad los sabores disponibles junto con los ingredientes adicionales y por último se obtiene la factura del pedido.
# Tabla de contenido
1. [Características](#características)
1. [Contenido del proyecto](#contenido-del-proyecto)
1. [Tecnologías](#tecnologías)
1. [IDE](#ide)
1. [Instalación](#instalación)
1. [Demo](#demo)
1. [Autores](#autores)
1. [Institucion](#institucion)

# Características
* Envió de datos entre las diferentes páginas de la aplicación.
* Lectura y consumo de datos de un archivo .JSON [ver .JSON](https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json).
* Desbordamiento de HTML con JavaScript.
* Implementación y manejo del framework Bootstrap.

# Contenido del proyecto

```
├── css/
│   ├── estilos.css
│   ├── header.css
│   ├── index.css
│   
├── html/
│   ├── factura.html
│   ├── opciones.html
│── images/
│   ├── demoPizza.gif
│   ├── logoPizzeria.gif
│── js/
│   ├── pizzas.js
├── index.html
└── README.md

```
### CSS
- [estilos.css](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/blob/master/css/estilo.css): Archivo CSS con estilos para el encabezado de las paginas.
- [header.css](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/blob/master/css/header.css): Archivo CSS con estilos para el tamaño del icono
- [index.css](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/blob/master/css/index.css): Archivo CSS con estilos en el diseño de la pagina princial

### HTML

- [factura.html](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/blob/master/html/factura.html): Archivo HTML que realiza la carga y visualizacion de los resultados de las operaciones.
- [opciones.html](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/blob/master/html/opciones.html): Archivo HTML que visualiza y carga las opciones del archivo json.

### Images

- [demoPizza.gif](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/blob/master/js/demoPizza.gif): Archivo Gif que contiene el demo de la aplicacion
- [logoPizzeria.gif](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/blob/master/images/logoPizzeria.gif): Archivo Gif que contiene el logo representativo de la aplicacion

### JS

- [pizzas.js](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/blob/master/js/pizzas.js): Archivo JSON que realiza la lectura de los datos, realiza las operaciones para el calculo de la factura y carga en el HTML

### Index
- [index.html](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/blob/master/index.html): Archivo HTML que contiene la pagina principal de la aplicacion.

# Tecnologías
* 🛠  HTML 5
* 🛠  CSS 3
* 🛠  JavaScript
* 🛠  [Boostrap 4](https://getbootstrap.com/) 

# IDE
El proyecto se desarrolla usando:
* 💻 [Suiblime Text](https://www.sublimetext.com/)
* 💻 [Atom](https://atom.io/)

# Instalación
##### **Clone the repository**
``` bash
$ git clone https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial.git
```
##### **Go into app's directory**
```bash
$ cd pizzeriaoficial
```
##### **Execute**
```bash
$ index.html 
```
# Demo
![Demo](https://gitlab.com/andrademanuelalejandroc/pizzeriaoficial/-/raw/master/images/demoPizza.gif)
# Autores 
* ⌨️ **Manuel Alejandro Coronel Anadrade** 
<br> Correo: andrademanuelalejandroc@ufps.edu.co
<br> Gitlab: [andrademanuelalejandroc](https://gitlab.com/andrademanuelalejandroc)
* ⌨️ **Ronald Eduardo Benitez Mejia** 
<br>correo: ronaldeduardobm@ufps.edu.co
<br> Gitlab: [ronaldeduardobm](https://gitlab.com/ronaldeduardobm)
# Institucion
[Universidad Fancisco de Paula Santander](https://ww2.ufps.edu.co/) - [Cúcuta / Colombia](https://www.google.com/maps/place/C%C3%BAcuta,+Norte+de+Santander/@7.9087586,-72.53944,13z/data=!3m1!4b1!4m5!3m4!1s0x8e66459c645dd28b:0x26736c1ff4db5caa!8m2!3d7.8890971!4d-72.4966896)